package com.kuma.utils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.Multimap;
import com.kuma.utils.collectors.Collectors2;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CollectionUtils2 extends org.apache.commons.collections.CollectionUtils {

	/**
	 * {@literal 將 Collection<E> 轉換成 List<R>}
	 * 
	 * @param            <E> 來源元素類型
	 * @param            <R> 目標元素類型
	 * @param collection 來源集合
	 * @param mapper     轉換方式
	 * @return mappedList
	 */
	public static final <E, R> List<R> mappingToList(Collection<E> collection, Function<E, R> mapper) {
		return collection.stream().map(mapper).collect(Collectors.toList());
	}

	/**
	 * {@literal 將 Collection<E> 轉換成 Set<R>}
	 * 
	 * @param            <E> 來源元素類型
	 * @param            <R> 目標元素類型
	 * @param collection 來源集合
	 * @param mapper     轉換方式
	 * @return mappedSet
	 */
	public static final <E, R> Set<R> mappingToSet(Collection<E> collection, Function<E, R> mapper) {
		return collection.stream().map(mapper).collect(Collectors.toSet());
	}

	/**
	 * {@literal 將 Collection<E> 轉換成 Map<K,V>，若 Key 有重複的話，會後蓋前}
	 * 
	 * @param             <E> 來源元素類型
	 * @param             <K> Map的Key類型
	 * @param             <V> Map的Value類型
	 * @param collection
	 * @param keyMapper
	 * @param valueMapper
	 * @return mappedMap
	 */
	public static final <E, K, V> Map<K, V> mappingToMap(Collection<E> collection, Function<E, K> keyMapper,
			Function<E, V> valueMapper) {
		return collection.stream().collect(Collectors.toMap(keyMapper, valueMapper, (o1, o2) -> o1));
	}

	/**
	 * {@literal 將 Collection<E> 轉換成 Map<K,V>，若 Key 有重複的話則會根據 mergeFunction 合併}
	 * 
	 * @param               <E> 來源元素類型
	 * @param               <K> Map的Key類型
	 * @param               <V> Map的Value類型
	 * @param collection
	 * @param keyMapper
	 * @param valueMapper
	 * @param mergeFunction
	 * @return mappedMap
	 */
	public static final <E, K, V> Map<K, V> mappingToMap(Collection<E> collection, Function<E, K> keyMapper,
			Function<E, V> valueMapper, BinaryOperator<V> mergeFunction) {
		return collection.stream().collect(Collectors.toMap(keyMapper, valueMapper, mergeFunction));
	}

	/**
	 * {@literal 將 Collection<E> 轉換成 Multimap<K,V>}
	 * 
	 * @param             <E> 來源元素類型
	 * @param             <K> Map的Key類型
	 * @param             <V> Map的Value類型
	 * @param collection
	 * @param keyMapper
	 * @param valueMapper
	 * @return mappedMultimap
	 */
	public static final <E, K, V> Multimap<K, V> mappingToMultiMap(Collection<E> collection, Function<E, K> keyMapper,
			Function<E, V> valueMapper) {
		return collection.stream().collect(Collectors2.toMultimap(keyMapper, valueMapper));
	}

	/**
	 * {@literal 將 Collection<E> 經過 predicate 過濾}
	 * 
	 * @param            <E> 來源元素類型
	 * @param collection 來源集合
	 * @param predicate  過濾方式
	 * @return filteredList
	 */
	public static final <E> List<E> filterToList(Collection<E> collection, Predicate<? super E> predicate) {
		return collection.stream().filter(predicate).collect(Collectors.toList());
	}

	/**
	 * {@literal 將 Collection<E> 經過 predicate 過濾}
	 * 
	 * @param            <E> 來源元素類型
	 * @param collection 來源集合
	 * @param predicate  過濾方式
	 * @return filteredSet
	 */
	public static final <E> Set<E> filterToSet(Collection<E> collection, Predicate<? super E> predicate) {
		return collection.stream().filter(predicate).collect(Collectors.toSet());
	}

	/**
	 * {@literal 將 Collection<E> 經過 mapper 攤平成 List<R>}
	 * 
	 * @param            <E> 來源元素類型
	 * @param            <R> 目標元素類型
	 * @param collection 來源集合
	 * @param mapper     轉換方式
	 * @return flatedList
	 */
	public static final <E, R> List<R> flatToList(Collection<E> collection,
			Function<? super E, ? extends Stream<? extends R>> mapper) {
		return collection.stream().flatMap(mapper).collect(Collectors.toList());
	}

	/**
	 * {@literal 將 Collection<E> 經過 mapper 攤平成 Set<R>}
	 * 
	 * @param            <E> 來源元素類型
	 * @param            <R> 目標元素類型
	 * @param collection 來源集合
	 * @param mapper     轉換方式
	 * @return flatedSet
	 */
	public static final <E, R> Set<R> flatToSet(Collection<E> collection,
			Function<? super E, ? extends Stream<? extends R>> mapper) {
		return collection.stream().flatMap(mapper).collect(Collectors.toSet());
	}

}
