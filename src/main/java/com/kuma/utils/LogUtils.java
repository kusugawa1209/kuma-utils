package com.kuma.utils;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.ArrayUtils;

import com.google.common.base.Stopwatch;
import com.google.common.base.Throwables;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LogUtils {

	/**
	 * log 錯誤訊息，並印出 stacktrace
	 * 
	 * @param t throwable
	 */
	public static void logThrowable(Throwable t) {
		logThrowable(t, true);
	}

	/**
	 * log 錯誤訊息，帶上額外訊息，並印出 stacktrace
	 * 
	 * @param t       throwable
	 * @param message 額外訊息
	 */
	public static void logThrowable(Throwable t, String message) {
		logThrowable(t, true, message);
	}

	/**
	 * log 錯誤訊息，帶上額外訊息及參數，並印出 stacktrace
	 * 
	 * @param t         throwable
	 * @param format    額外訊息的pattern
	 * @param arguments 額外訊息裡要帶的參數
	 */
	public static void logThrowable(Throwable t, String format, Object... arguments) {
		logThrowable(t, true, format, arguments);
	}

	/**
	 * log 錯誤訊息，由 enablePrintStackTrace 決定是否要印 stacktrace
	 * 
	 * @param t                     throwable
	 * @param enablePrintStackTrace 是否要印 stacktrace
	 */
	public static void logThrowable(Throwable t, boolean enablePrintStackTrace) {
		log.warn("Encounter {}, cause: {}", t.getClass().getSimpleName(), t.toString());
		printStackTraceIfEnable(t, enablePrintStackTrace);
	}

	/**
	 * log 錯誤訊息，帶上額外訊息，並由 enablePrintStackTrace 決定是否要印 stacktrace
	 * 
	 * @param t                     throwable
	 * @param enablePrintStackTrace 是否要印 stacktrace
	 * @param message               額外訊息
	 */
	public static void logThrowable(Throwable t, boolean enablePrintStackTrace, String message) {
		log.warn("{}, encounter {}, cause: {}", message, t.getClass().getSimpleName(), t.toString());
		printStackTraceIfEnable(t, enablePrintStackTrace);
	}

	/**
	 * log 錯誤訊息，帶上額外訊息及參數，並由 enablePrintStackTrace 決定是否要印 stacktrace
	 * 
	 * @param t                     throwable
	 * @param enablePrintStackTrace 是否要印 stacktrace
	 * @param format                額外訊息的pattern
	 * @param arguments             額外訊息裡要帶的參數
	 */
	public static void logThrowable(Throwable t, boolean enablePrintStackTrace, String format, Object... arguments) {
		log.warn(format + ", encounter {}, cause: {}", arguments, t.getClass().getSimpleName(), t.toString());
		printStackTraceIfEnable(t, enablePrintStackTrace);
	}

	/**
	 * 啟動計時器 watch ，並根據 format & arguments log 訊息
	 * 
	 * @param watch     計時器
	 * @param format    訊息 pattern
	 * @param arguments 訊息參數
	 */
	public static void logStart(Stopwatch watch, String format, Object... arguments) {
		if (watch.isRunning()) {
			watch.stop();
		}

		watch.reset();
		watch.start();

		log.info(format, arguments);
	}

	/**
	 * 啟動計時器 watch ，並 log 訊息
	 * 
	 * @param watch   計時器
	 * @param message 訊息
	 */
	public static void logStart(Stopwatch watch, String message) {
		if (watch.isRunning()) {
			watch.stop();
		}

		watch.reset();
		watch.start();

		log.info(message);
	}

	/**
	 * 根據 format & arguments log 訊息，並回傳一個已啟動計時的計時器
	 * 
	 * @param format    訊息格式
	 * @param arguments 訊息參數
	 * @return 已啟動的計時器
	 */
	public static Stopwatch logStart(String format, Object... arguments) {
		Stopwatch watch = Stopwatch.createStarted();

		log.info(format, arguments);

		return watch;
	}

	/**
	 * log 訊息，並回傳一個已啟動計時的計時器
	 * 
	 * @param message 訊息
	 * @return 已啟動的計時器
	 */
	public static Stopwatch logStart(String message) {
		Stopwatch watch = Stopwatch.createStarted();

		log.info(message);

		return watch;
	}

	/**
	 * 根據 format & arguments log 訊息及計時器經過的時間
	 * 
	 * @param watch     計時器
	 * @param format    訊息格式
	 * @param arguments 訊息參數
	 */
	public static void logStop(Stopwatch watch, String format, Object... arguments) {
		watch.stop();

		String formatWithTime = format + ", costs {} mills.";
		Object[] argumentsWithTime = ArrayUtils.add(arguments, watch.elapsed(TimeUnit.MILLISECONDS));

		log.info(formatWithTime, argumentsWithTime);
	}

	/**
	 * log 訊息及計時器經過的時間
	 * 
	 * @param watch   計時器
	 * @param message 訊息
	 */
	public static void logStop(Stopwatch watch, String message) {
		watch.stop();

		String messageWithTime = message + ", costs {} mills.";

		log.info(messageWithTime, watch.elapsed(TimeUnit.MILLISECONDS));
	}

	/**
	 * if enablePrintStackTrace = true, then print stack trace
	 * 
	 * @param t
	 * @param enablePrintStackTrace
	 */
	private static void printStackTraceIfEnable(Throwable t, boolean enablePrintStackTrace) {
		if (enablePrintStackTrace) {
			log.warn(Throwables.getStackTraceAsString(t));
		}
	}
}
