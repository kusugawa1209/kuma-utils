package com.kuma.utils;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JsonUtils {

	private static Gson gson;

	static {
		GsonBuilder builder = new GsonBuilder();
		gson = builder.create();
	}

	/**
	 * 將 model 轉為 clazz 的型別.
	 * 
	 * @param model
	 * @param clazz
	 * @return coverted model
	 */
	public static <T> T convert(final Object model, final Class<T> clazz) {
		if (Objects.isNull(model)) {
			return null;
		}

		if (model instanceof String) {
			return toObject(model.toString(), clazz);
		} else {
			String jsonString = toJsonString(model);

			return toObject(jsonString, clazz);
		}
	}

	/**
	 * 將 sources 轉成另外一包 clazz.
	 * 
	 * @param sources
	 * @param clazz
	 * @return converted list of models
	 */
	public static <T> List<T> convert(final Collection<?> sources, final Class<T> clazz) {
		if (CollectionUtils.isEmpty(sources)) {
			return Lists.newArrayList();
		}

		return sources.stream().map(source -> convert(source, clazz)).collect(Collectors.toList());
	}

	/**
	 * 將 model 轉為 json string.
	 * 
	 * @param model
	 * @return json string
	 */
	public static String toJsonString(Object model) {
		return gson.toJson(model);
	}

	/**
	 * 將 json string 轉為 clazz 型別的物件
	 * 
	 * @param jsonString
	 * @param clazz
	 * @return object
	 */
	public static <T> T toObject(String jsonString, Class<T> clazz) {
		return gson.fromJson(jsonString, clazz);
	}

	/**
	 * 將 json string 轉為一包 clazz 型別的物件
	 * 
	 * @param jsonString
	 * @param clazz
	 * @return list of objects
	 */
	public static <T> List<T> convertToList(String jsonString, Class<T> clazz) {
		if (StringUtils.isBlank(jsonString)) {
			return Lists.newArrayList();
		}

		List<?> objs = toObject(jsonString, List.class);
		return convert(objs, clazz);
	}

}
