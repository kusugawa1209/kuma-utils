package com.kuma.utils;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.kuma.utils.exception.RetrieveFailureException;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConnectionUtils {

	static {
		try {
			SSLContext sc = getSSLContext();
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier((String hostname, SSLSession session) -> true);
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			LogUtils.logThrowable(e);
		}
	}

	public static HttpURLConnection createHttpConnection(String uri, HttpMethod httpMethod,
			Optional<MediaType> acceptMediaType, Optional<MediaType> contentMediaType, Optional<String> authToken,
			Optional<Object> data) throws IOException {

		URL url = new URL(uri);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod(httpMethod.name());
		acceptMediaType.ifPresent(mediaType -> conn.setRequestProperty("Accept", mediaType.toString()));
		contentMediaType.ifPresent(mediaType -> conn.setRequestProperty("Content-Type", mediaType.toString()));
		authToken.ifPresent(token -> conn.setRequestProperty("Authorization", "Basic " + token));

		data.ifPresent((Object d) -> {
			conn.setDoOutput(true);
			try (DataOutputStream dos = new DataOutputStream(conn.getOutputStream())) {
				String jsonParam = JsonUtils.toJsonString(d);
				dos.writeBytes(jsonParam);
				dos.flush();
			} catch (IOException e) {
				LogUtils.logThrowable(e);
			}
		});

		return conn;
	}

	public static HttpsURLConnection createHttpsConnection(String uri, HttpMethod httpMethod,
			Optional<MediaType> acceptMediaType, Optional<MediaType> contentMediaType, Optional<String> authToken,
			Optional<Object> data) throws IOException {

		URL url = new URL(uri);
		HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
		conn.setRequestMethod(httpMethod.name());
		acceptMediaType.ifPresent(mediaType -> conn.setRequestProperty("Accept", mediaType.toString()));
		contentMediaType.ifPresent(mediaType -> conn.setRequestProperty("Content-Type", mediaType.toString()));
		authToken.ifPresent(token -> conn.setRequestProperty("Authorization", "Basic " + token));

		data.ifPresent((Object d) -> {
			conn.setDoOutput(true);
			try (DataOutputStream dos = new DataOutputStream(conn.getOutputStream())) {
				String jsonParam = JsonUtils.toJsonString(d);
				dos.writeBytes(jsonParam);
				dos.flush();
			} catch (IOException e) {
				LogUtils.logThrowable(e);
			}
		});

		return conn;
	}

	public static <T> T retrieveModelFromConnection(HttpURLConnection connection,
			Collection<HttpStatus> acceptedStatuses, Class<T> returnModelType) throws RetrieveFailureException {
		Set<Integer> statusCodes = CollectionUtils2.mappingToSet(acceptedStatuses, HttpStatus::value);
		try {
			int responseCode = connection.getResponseCode();
			if (statusCodes.contains(responseCode)) {
				String output = IOUtils.toString(connection.getInputStream(), StandardCharsets.UTF_8);
				return JsonUtils.toObject(output, returnModelType);
			} else {
				throw new RetrieveFailureException(
						"Can't retrieve result from connection, status code is: " + responseCode);
			}
		} catch (IOException e) {
			LogUtils.logThrowable(e);
			throw new RetrieveFailureException("Can't retrieve result from connection.", e);
		}
	}

	private static SSLContext getSSLContext() throws NoSuchAlgorithmException, KeyManagementException {
		SSLContext sc = SSLContext.getInstance("TLSv1.2");
		TrustManager[] allTrustManager = getAllTrustManager();
		sc.init(null, allTrustManager, new java.security.SecureRandom());

		return sc;
	}

	private static TrustManager[] getAllTrustManager() {
		return new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
				// Trust all certifications
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
				// Trust all certifications
			}
		} };
	}

}
