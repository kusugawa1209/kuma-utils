package com.kuma.utils.collectors;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MultimapCollector<T, K, V> implements Collector<T, Multimap<K, V>, Multimap<K, V>> {

	private Function<? super T, ? extends K> keyMapper;

	private Function<? super T, ? extends V> valueMapper;

	@Override
	public BiConsumer<Multimap<K, V>, T> accumulator() {
		return (Multimap<K, V> map, T element) -> map.put(keyMapper.apply(element), valueMapper.apply(element));
	}

	@Override
	public Set<Characteristics> characteristics() {
		return Collections.unmodifiableSet(EnumSet.of(Collector.Characteristics.UNORDERED));
	}

	@Override
	public BinaryOperator<Multimap<K, V>> combiner() {
		return (Multimap<K, V> map1, Multimap<K, V> map2) -> {
			map1.putAll(map2);

			return map1;
		};
	}

	@Override
	public Function<Multimap<K, V>, Multimap<K, V>> finisher() {
		return Function.identity();
	}

	@Override
	public Supplier<Multimap<K, V>> supplier() {
		return LinkedListMultimap::create;
	}

}
