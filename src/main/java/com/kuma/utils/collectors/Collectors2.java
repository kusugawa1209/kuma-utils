package com.kuma.utils.collectors;

import java.util.function.Function;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Collectors2 {

	/**
	 * 提供 Multimap 的 collector
	 * 
	 * @param keyMapper
	 * @param valueMapper
	 * @return MultimapCollector
	 */
	public static <T, K, V> MultimapCollector<T, K, V> toMultimap(Function<? super T, ? extends K> keyMapper,
			Function<? super T, ? extends V> valueMapper) {
		return new MultimapCollector<>(keyMapper, valueMapper);
	}

}
