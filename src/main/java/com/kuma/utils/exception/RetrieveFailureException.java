package com.kuma.utils.exception;

public class RetrieveFailureException extends Exception {

	private static final long serialVersionUID = 1L;

	public RetrieveFailureException(String arg0) {
		super(arg0);
	}

	public RetrieveFailureException(Throwable arg0) {
		super(arg0);
	}

	public RetrieveFailureException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
