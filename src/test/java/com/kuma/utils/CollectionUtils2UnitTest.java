package com.kuma.utils;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.kuma.utils.vo.Person;

public class CollectionUtils2UnitTest {

	private List<Person> persons = Lists.newArrayList();

	private List<List<Person>> nestedPersons = Lists.newArrayList();

	@Before
	public void setup() {
		persons.add(new Person(1L, "Person 1a"));
		persons.add(new Person(1L, "Person 1b"));
		persons.add(new Person(2L, "Person 2"));

		nestedPersons.add(persons);
		nestedPersons.add(persons);
	}

	@Test
	public void testMappingToList() {
		List<Long> personIds = CollectionUtils2.mappingToList(persons, Person::getId);
		Assert.assertEquals("Size of personIds should be 3", 3, personIds.size());
		Assert.assertTrue("Index 0 of personIds should be 1L", personIds.get(0).equals(1L));
		Assert.assertTrue("Index 1 of personIds should be 1L", personIds.get(1).equals(1L));
		Assert.assertTrue("Index 2 of personIds should be 2L", personIds.get(2).equals(2L));
	}

	@Test
	public void testMappingToSet() {
		Set<Long> personIds = CollectionUtils2.mappingToSet(persons, Person::getId);
		Assert.assertEquals("Size of personIds should be 2", 2, personIds.size());
		Assert.assertTrue("PersonIds should contain 1L", personIds.contains(1L));
		Assert.assertTrue("PersonIds should contain 2L", personIds.contains(2L));
	}

	@Test
	public void testMappingToMap() {
		Map<Long, String> idsAndNames = CollectionUtils2.mappingToMap(persons, Person::getId, Person::getName);
		Assert.assertEquals("Size of idsAndNames should be 2", 2, idsAndNames.size());
	}

	@Test
	public void testMappingToMapWithMergeFunction() {
		Map<Long, String> idsAndNames = CollectionUtils2.mappingToMap(persons, Person::getId, Person::getName,
				(name1, name2) -> name1 + " and " + name2);
		Assert.assertEquals("Size of idsAndNames should be 2", 2, idsAndNames.size());
		Assert.assertEquals("Name of id1 should be Person 1a and Person 1b", "Person 1a and Person 1b",
				idsAndNames.get(1L));
	}

	@Test
	public void testMappingToMuitiMap() {
		List<Person> myPersons = Lists.newArrayList();
		myPersons.add(new Person(1L, "Person 1-1"));
		myPersons.add(new Person(1L, "Person 1-2"));
		myPersons.add(new Person(2L, "Person 2"));
		myPersons.add(new Person(2L, "Person 2"));
		
		Multimap<Long, String> idsAndNames = CollectionUtils2.mappingToMultiMap(myPersons, Person::getId,
				Person::getName);
		
		Assert.assertEquals("Size of idsAndNames should be 4", 4, idsAndNames.size());
		Assert.assertEquals("Size of names of id 1 should be 2", 2, idsAndNames.get(1L).size());

		Assert.assertEquals("Size of names of id 2 should be 2", 2, idsAndNames.get(2L).size());
	}

	@Test
	public void testFilterToList() {
		List<Person> filteredPersons = CollectionUtils2.filterToList(persons, person -> person.getId().equals(1L));
		Assert.assertEquals("Size of filteredPersons should be 2", 2, filteredPersons.size());
		Assert.assertTrue("Id of index 0 of filteredPersons should be 1L", filteredPersons.get(0).getId().equals(1L));
		Assert.assertTrue("Id of index 1 of filteredPersons should be 1L", filteredPersons.get(1).getId().equals(1L));
	}

	@Test
	public void testFilterToSet() {
		Set<Person> filteredPersons = CollectionUtils2.filterToSet(persons, person -> person.getId().equals(1L));
		Assert.assertEquals("Size of filteredPersons should be 2", 2, filteredPersons.size());
		Assert.assertTrue("FilteredPersons should contain person 1a", filteredPersons.contains(persons.get(0)));
		Assert.assertTrue("FilteredPersons should contain person 1b", filteredPersons.contains(persons.get(1)));
	}

	@Test
	public void testFlatToList() {
		List<Person> flatedPersons = CollectionUtils2.flatToList(nestedPersons, List::stream);
		Assert.assertEquals("Size of flatedPersons should be 6", 6, flatedPersons.size());
	}

	@Test
	public void testFlatToSet() {
		Set<Person> flatedPersons = CollectionUtils2.flatToSet(nestedPersons, List::stream);
		Assert.assertEquals("Size of flatedPersons should be 3", 3, flatedPersons.size());
	}
}
