package com.kuma.utils;

import org.junit.Test;

import com.google.common.base.Stopwatch;

public class LogUtilsUnitTest {

	@Test
	public void testLogStartAndStop() throws InterruptedException {
		Stopwatch watch = LogUtils.logStart("Start test");
		Thread.sleep(1000);
		LogUtils.logStop(watch, "Test");
	}
}
