package com.kuma.utils;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.Lists;
import com.kuma.utils.vo.Person;

public class JsonUtilsUnitTest {

	private static final String ASSERTION_MESSAGE = "Converted person should be as the same as original one.";

	@Test
	public void testConvertMulti() {
		List<Person> persons = Lists.newArrayList();
		persons.add(new Person(1L, "Person 1"));
		persons.add(new Person(2L, "Person 2"));

		List<Person> convertedPersons = JsonUtils.convert(persons, Person.class);

		Assert.assertEquals(ASSERTION_MESSAGE, persons.get(0), convertedPersons.get(0));
		Assert.assertEquals(ASSERTION_MESSAGE, persons.get(1), convertedPersons.get(1));
	}

	@Test
	public void testConvertSingle() {
		Person person = new Person(1L, "Person 1");

		Person convertedPerson = JsonUtils.convert(person, Person.class);

		Assert.assertEquals(ASSERTION_MESSAGE, person, convertedPerson);
	}

	@Test
	public void testConvertToJsonAndFromJson() {
		Person person = new Person(1L, "Person 1");
		String jsonString = JsonUtils.toJsonString(person);
		Person convertedPerson1 = JsonUtils.convert(jsonString, Person.class);
		Person convertedPerson2 = JsonUtils.toObject(jsonString, Person.class);

		Assert.assertEquals(ASSERTION_MESSAGE, person, convertedPerson1);
		Assert.assertEquals(ASSERTION_MESSAGE, person, convertedPerson2);
	}

}
