package com.kuma.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.google.common.collect.Sets;
import com.kuma.utils.exception.RetrieveFailureException;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConnectionUtilsUnitTest {

	@Test
	public void testCreateHttpConnection() throws IOException {

		String uri = "https://mis.twse.com.tw/stock/api/getStockInfo.jsp?ex_ch=tse_2316.tw&json=1&delay=0&_="
				+ (new Date()).getTime();

		HttpURLConnection connection = ConnectionUtils.createHttpConnection(uri, HttpMethod.GET,
				Optional.of(MediaType.APPLICATION_JSON), Optional.of(MediaType.APPLICATION_JSON), Optional.empty(),
				Optional.empty());

		if (connection.getResponseCode() != HttpStatus.OK.value()) {
			log.error("Failed : HTTP error code : {}", connection.getResponseCode());

			throw new IOException(connection.getResponseMessage());
		} else {
			String output = IOUtils.toString(connection.getInputStream(), StandardCharsets.UTF_8);
			Assert.assertTrue("Output should not be empty.", StringUtils.isNotBlank(output));
		}
	}

	@Test
	public void testRetrieveModelFromConnection() throws IOException, RetrieveFailureException {
		String uri = "https://mis.twse.com.tw/stock/api/getStockInfo.jsp?ex_ch=tse_2316.tw&json=1&delay=0&_="
				+ (new Date()).getTime();

		HttpURLConnection connection = ConnectionUtils.createHttpConnection(uri, HttpMethod.GET,
				Optional.of(MediaType.APPLICATION_JSON), Optional.of(MediaType.APPLICATION_JSON), Optional.empty(),
				Optional.empty());

		StockInfo stockInfo = ConnectionUtils.retrieveModelFromConnection(connection,
				Sets.newHashSet(HttpStatus.OK, HttpStatus.FOUND), StockInfo.class);
		Assert.assertEquals("Stock name should be 楠梓電.", "楠梓電", stockInfo.getName());
		Assert.assertEquals("Stock code should be 2316.", 2316, stockInfo.getCode());
	}

	@AllArgsConstructor
	class StockInfo {

		private List<Map<String, String>> msgArray;

		public String getName() {
			return msgArray.get(0).get("n");
		}

		public int getCode() {
			return Integer.valueOf(msgArray.get(0).get("c"));
		}

		public double getCurrentPrice() {
			return Double.valueOf(msgArray.get(0).get("ob"));
		}

	}
}
