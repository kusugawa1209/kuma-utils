package com.kuma.utils;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Maps;
import com.kuma.utils.vo.Person;

public class MapUtils2UnitTest {

	private Map<String, Person> persons = Maps.newHashMap();

	private Map<String, Map<String, Person>> nestedPersons = Maps.newHashMap();

	@Before
	public void setup() {
		persons.put("Person 1", new Person(1L, "Person 1"));

		nestedPersons.put("Parent person 1", persons);
	}

	@Test
	public void testGetValue() {
		Person person = MapUtils2.getValue(persons, "Person 1");

		Assert.assertNotNull("person should not be null", person);
		Assert.assertEquals("the name of the person should be Person 1", "Person 1", person.getName());

		Person nestedPerson = MapUtils2.getValue(nestedPersons, "Parent person 1.Person 1");

		Assert.assertNotNull("person should not be null", nestedPerson);
		Assert.assertEquals("the name of the person should be Person 1", "Person 1", nestedPerson.getName());
	}

}